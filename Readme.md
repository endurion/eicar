# Eicar

Projet de création de virus sur la base de virus désactivé pour tester les antivirus.



## Virus Eicar

Ce virus n'en est pas un, ce n'est qu'un fichier texte contenant une signature connu par tous les antivirus.

- Fichier [eicar.txt](./resources/infected/eicar.txt)

- [Wikipedia Eicar](https://fr.wikipedia.org/wiki/Fichier_de_test_Eicar)

==> **Virustotal** : 64/67 ( [link](https://www.virustotal.com/gui/file/275a021bbfb6489e54d471899f7db9d1663fc695ec2fe2a2c4538aabf651fd0f/detection))

## Format Word

### AutoOpen

Un M$ Word en AutoOpen qui crée le fichier Eicar dans le premier répertoire disponible en écriture, ou sinon dans `C:\Temp`. Et sa version embedded dans un PDF.

- Fichier [eicar.doc](./resources/infected/eicar.doc)
- Méthodologie : [video Didier Stevens](https://videos.didierstevens.com/2015/08/31/the-making-of-pdf-with-embedded-doc-dropping-eicar/)



==> **Virustotal** : 11/63 ( [link](https://www.virustotal.com/gui/file/2b8dfb6f29a85abd9e4ec5a16959114cf266538d8b43d4dbce1fd29064bfe29f/detection))

### Embedded

Un fichier Word étant un zip, on rajoute dedans eicar.txt.

~~~bash
cp resources/empty.docx generated/eicar.docx
7zip a generated/eicar.docx resources/infected/eicar.txt 
~~~



==> **Virustotal** : 51/66 ( [link](https://www.virustotal.com/gui/file/cc3c6999817ae6e866f2f214f9490deb6abb385d8a9a5fe3791cb61fcd6edcb6/detection))

## Format PDF

### AutoOpen

On crée un PDF qui va autoopen un virus. Dans notre cas, vu que Eicar n'est pas exécutable, on va le faire ouvrir la version Word en Autoopen.

~~~bash
python make-pdf-embedded.py -a resources/infected/eicar.doc generated/eicar.pdf
~~~



==> **Virustotal** : 18/62 ( [link](https://www.virustotal.com/gui/file/6195841df58923c5ebc4fa6c075abe8ea96ffa26b4ba747c0ee61282536dab67/detection))

## Format PNG

### Stéganographie

On va concaténer 2 fichiers en 1.

~~~bash
# CMD Windows
copy /b resources\logo_eicar.png+resources\infected\eicar.txt generated\eicar.png
~~~

- Fichier [logo_eicar.png](./resources/logo_eicar.png)

==> **Virustotal** : 09/57 ( [link](https://www.virustotal.com/gui/file/b34e1b0b960ec290cf216727cfd2ba111d4a02d010efab0a8e5fbc025f8f4f77/detection))



### Exif

L'autre possibilité, c'est d'injecter dans les commentaires de l'image le virus.

~~~bash
cp resources/1pixel.png generated/eicar_exif.png
exiftool "-comment<=resources/infected/eicar.txt" generated/eicar_exif.png
~~~

==> **Virustotal** : 05/58 ( [link](https://www.virustotal.com/gui/file/22c13da5759403609368acb325f1d51b75a067fbd2000d518ae9d89371385040/detection))



### Masquerading

Faire passer un fichier pour un autre type.

~~~bash
curl --data-binary @filename -H 'Content-Type: application/json' http://example.com/
~~~



